package com.morozov.tm.service;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.api.ITaskService;
import com.morozov.tm.dto.TaskDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public final class TaskService implements ITaskService {
    @Nullable
    final private EntityManagerFactory factory;

    public TaskService(@Nullable EntityManagerFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Task> findAllTask() throws ConnectionLostException {
        @NotNull List<Task> taskList;
        @Nullable final EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            taskList = taskRepository.findAll();
        } finally {
            em.close();
        }
        return taskList;
    }

    @Override
    public List<Task> getAllTaskByUserId(@NotNull final String userId)
            throws ConnectionLostException {
        @Nullable final EntityManager em = getEntityManager();
        @NotNull List<Task> taskList;
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            taskList = taskRepository.findAllByUserId(userId);
        } finally {
            em.close();
        }
        return taskList;
    }

    @Override
    public Task addTask(final User user, @NotNull final String taskName, final Project project)
            throws StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(taskName);
        @Nullable final EntityManager em = getEntityManager();
        @NotNull final Task task;
        task = new Task();
        task.setName(taskName);
        task.setProject(project);
        task.setUser(user);
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.persist(task);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
        return task;
    }

    @Override
    public boolean removeTaskById(@NotNull final String userId, @NotNull final String id)
            throws StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(id);
        @Nullable final EntityManager em = getEntityManager();
        boolean isDeleted;
        isDeleted = false;
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);

            Task oneByUserId = taskRepository.findOneByUserId(userId, id);
            if (oneByUserId != null) {
                taskRepository.remove(oneByUserId);
                em.getTransaction().commit();
                isDeleted = true;
            }
        } finally {
            em.close();
        }
        return isDeleted;
    }

    @Override
    public void updateTask(
            final User user, @NotNull final String id, @NotNull final String name, @NotNull final String description,
            @NotNull final String dataStart, @NotNull final String dataEnd, @NotNull final Project project)
            throws StringEmptyException, ParseException, TaskNotFoundException, ConnectionLostException {
        @Nullable final EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            DataValidationUtil.checkEmptyString(id, name, description);
            final Task task = taskRepository.findOne(id);
            if (task == null) throw new TaskNotFoundException();
            task.setId(id);
            task.setName(name);
            task.setDescription(description);
            final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
            if (updatedStartDate != null) {
                task.setStatus(StatusEnum.PROGRESS);
            }
            task.setStartDate(updatedStartDate);
            final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
            if (updatedEndDate != null) {
                task.setStatus(StatusEnum.READY);
            }
            task.setEndDate(updatedEndDate);
            task.setProject(project);
            task.setUser(user);
            taskRepository.merge(task);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public List<Task> getAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(projectId);
        @Nullable final EntityManager em = getEntityManager();
        @NotNull List<Task> resultTaskList;
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        } finally {
            em.close();
        }
        return resultTaskList;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws ConnectionLostException {
        @Nullable final EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.deleteAllTaskByProjectId(userId, projectId);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public void clearTaskList() throws ConnectionLostException {
        @Nullable final EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.removeAll();
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            throws ConnectionLostException {
        @Nullable final EntityManager em = getEntityManager();
        @NotNull List<Task> taskListByProjectId;
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            if (string.isEmpty()) return taskRepository.findAllByUserId(userId);
            taskListByProjectId = taskRepository.findTaskByStringInNameOrDescription(userId, string);
        } finally {
            em.close();
        }
        return taskListByProjectId;
    }

    @Override
    public void removeAllTaskByUserId(@NotNull final String userId) throws ConnectionLostException {
        @Nullable final EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.removeAllByUserId(userId);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @NotNull
    private EntityManager getEntityManager() throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        return em;
    }

    @Override
    public void loadTaskList(@Nullable final List<Task> loadList) throws ConnectionLostException {
        if (loadList != null) {
            for (Task task : loadList) {
                @Nullable final EntityManager em = getEntityManager();
                try {
                    em.getTransaction().begin();
                    @Nullable final ITaskRepository taskRepository = new TaskRepository(em);
                    taskRepository.persist(task);
                    em.getTransaction().commit();
                } finally {
                    em.close();
                }
            }
        }
    }

    @Override
    public @NotNull TaskDto transferTaskToTaskDto(@NotNull Task task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setCreatedData(task.getCreatedData());
        taskDto.setStartDate(task.getStartDate());
        taskDto.setEndDate(task.getEndDate());
        taskDto.setStatus(task.getStatus());
        if (task.getUser() != null) taskDto.setUserId(task.getUser().getId());
        if (task.getProject() != null) taskDto.setIdProject(task.getProject().getId());
        return taskDto;
    }

    @Override
    public Task transferTaskDtoToTask(@NotNull TaskDto taskDto, @NotNull final User user, @NotNull final Project project) {
        @NotNull final Task task = new Task();
        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setCreatedData(taskDto.getCreatedData());
        task.setStartDate(taskDto.getStartDate());
        task.setEndDate(taskDto.getEndDate());
        task.setStatus(taskDto.getStatus());
        task.setUser(user);
        task.setProject(project);
        return task;
    }

    @Override
    public @NotNull List<TaskDto> transferListTaskToListTaskDto(@NotNull List<Task> taskList) {
        List<TaskDto> taskDtoList = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            taskDtoList.add(transferTaskToTaskDto(task));
        }
        return taskDtoList;
    }
}

