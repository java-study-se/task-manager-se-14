package com.morozov.tm.api;


import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAllByUserId(@NotNull final String userId);

    Project findOneByUserId(@NotNull final String userId, @NotNull final String id);

    List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            ;

    void removeAllByUserId(@NotNull final String userId);
}
