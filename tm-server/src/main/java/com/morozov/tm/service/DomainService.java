package com.morozov.tm.service;

import com.morozov.tm.api.IDomainService;
import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.dto.DomainDto;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;



public class DomainService implements IDomainService {
    protected IServiceLocator serviceLocator;

    public DomainService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(@NotNull final DomainDto domainDto) throws ConnectionLostException {
        serviceLocator.getProjectService().loadProjectList(domainDto.getProjectList());
        serviceLocator.getTaskService().loadTaskList(domainDto.getTaskList());
        serviceLocator.getUserService().loadUserList(domainDto.getUserList());
    }

    @Override
    public void export(@NotNull final DomainDto domainDto) throws ConnectionLostException {
        domainDto.setProjectList(serviceLocator.getProjectService().findAllProject());
        domainDto.setTaskList(serviceLocator.getTaskService().findAllTask());
        domainDto.setUserList(serviceLocator.getUserService().findAllUser());
    }
}
