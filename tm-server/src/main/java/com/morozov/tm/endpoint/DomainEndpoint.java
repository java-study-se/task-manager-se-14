package com.morozov.tm.endpoint;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.dto.DomainDto;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class DomainEndpoint {
    private IServiceLocator serviceLocator;
    private ISessionService sessionService;
    private IUserService userService;

    public DomainEndpoint(Bootstrap bootstrap) {
        this.serviceLocator = bootstrap;
        this.sessionService = bootstrap.getSessionService();
        this.userService = bootstrap.getUserService();
    }

    @WebMethod
    public void load(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "domain") final DomainDto domainDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        serviceLocator.getDomainService().load(domainDto);

    }

    @WebMethod
    public void export(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "domain") final DomainDto domainDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        serviceLocator.getDomainService().export(domainDto);
    }
}
