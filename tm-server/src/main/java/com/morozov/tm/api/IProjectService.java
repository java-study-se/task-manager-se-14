package com.morozov.tm.api;


import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    List<Project> findAllProject() throws ConnectionLostException;
    Project findOneById(@NotNull final String projectId) throws ConnectionLostException;

    List<Project> getAllProjectByUserId(@NotNull String userId) throws ConnectionLostException;

    Project addProject(@NotNull String projectName, User user) throws StringEmptyException, ConnectionLostException;

    boolean removeProjectById(@NotNull String userId, @NotNull String id) throws StringEmptyException, ConnectionLostException;

    void updateProject(
            @NotNull String id, @NotNull String projectName, @NotNull String projectDescription,
            @NotNull String dataStart, @NotNull String dataEnd, @NotNull User user)
            throws StringEmptyException, ParseException, ProjectNotFoundException, ConnectionLostException;

    List<Project> findProjectByStringInNameOrDescription(@NotNull String userId, @NotNull String string)
            throws ConnectionLostException;

    void removeAllByUserId(@NotNull String userId) throws ConnectionLostException;

    void clearProjectList() throws ConnectionLostException;

    void loadProjectList(@Nullable final List<Project> loadProjectList) throws ConnectionLostException;

    @NotNull
    ProjectDto transferProjectToProjectDto(@NotNull final Project project);

    @NotNull
    Project transferProjectDtoToProject(@NotNull final ProjectDto projectDto, @NotNull final User user);

    @NotNull
    List<ProjectDto> transferListProjectToListProjectDto(@NotNull final List<Project> projectList);
}
