package com.morozov.tm.api;


import com.morozov.tm.entity.Session;
public interface ISessionRepository extends IRepository<Session> {
}
