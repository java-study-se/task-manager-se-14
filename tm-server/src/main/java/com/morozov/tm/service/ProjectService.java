package com.morozov.tm.service;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.api.IProjectService;
import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectService implements IProjectService {
    @Nullable
    final private EntityManagerFactory factory;

    public ProjectService(@Nullable EntityManagerFactory factory) {
        this.factory = factory;
    }

    @Override
    public final List<Project> findAllProject() throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            return projectRepository.findAll();
        } finally {
            em.close();
        }
    }

    @Override
    public Project findOneById(@NotNull String projectId) throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            return projectRepository.findOne(projectId);
        } finally {
            em.close();
        }
    }

    @Override
    public final List<Project> getAllProjectByUserId(@NotNull String userId) throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            return projectRepository.findAllByUserId(userId);
        } finally {
            em.close();
        }
    }

    @Override
    public final Project addProject(@NotNull final String projectName, @NotNull final User user)
            throws StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(projectName);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUser(user);
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            projectRepository.persist(project);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
        return project;
    }

    @Override
    public final boolean removeProjectById(@NotNull final String userId, @NotNull String id)
            throws StringEmptyException, ConnectionLostException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            @Nullable final Project removeProject = projectRepository.findOneByUserId(userId, id);
            if (removeProject != null) {
                projectRepository.remove(removeProject);
                em.getTransaction().commit();
                isDeleted = true;
            }
        } finally {
            em.close();
        }
        return isDeleted;
    }

    @Override
    public final void updateProject(
            @NotNull final String id, @NotNull final String projectName, @NotNull final String projectDescription,
            @NotNull final String dataStart, @NotNull final String dataEnd, @NotNull final User user)
            throws ParseException, ProjectNotFoundException, ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            final Project project = projectRepository.findOne(id);
            if (project == null) throw new ProjectNotFoundException();
            project.setId(id);
            project.setName(projectName);
            project.setDescription(projectDescription);
            final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
            if (updatedStartDate != null) {
                project.setStatus(StatusEnum.PROGRESS);
            }
            project.setStartDate(updatedStartDate);
            final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
            if (updatedEndDate != null) {
                project.setStatus(StatusEnum.READY);
            }
            project.setEndDate(updatedEndDate);
            project.setUser(user);
            projectRepository.merge(project);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public final List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            throws ConnectionLostException {
        @NotNull List<Project> projectListByUserId;
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        em.getTransaction().begin();
        @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
        if (string.isEmpty()) return projectRepository.findAllByUserId(userId);
        projectListByUserId = projectRepository.findProjectByStringInNameOrDescription(userId, string);
        return projectListByUserId;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            projectRepository.removeAllByUserId(userId);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public final void clearProjectList() throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
            projectRepository.removeAll();
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public void loadProjectList(@Nullable List<Project> loadProjectList) throws ConnectionLostException {
        if (loadProjectList != null) {
            for (Project project : loadProjectList) {
                if (factory == null) throw new ConnectionLostException();
                @Nullable final EntityManager em = factory.createEntityManager();
                if (em == null) throw new ConnectionLostException();
                try {
                    em.getTransaction().begin();
                    @Nullable final IProjectRepository projectRepository = new ProjectRepository(em);
                    projectRepository.persist(project);
                    em.getTransaction().commit();
                } finally {
                    em.close();
                }
            }
        }
    }

    @NotNull
    @Override
    public ProjectDto transferProjectToProjectDto(@NotNull final Project project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setUserId(project.getUser().getId());
        projectDto.setCreatedData(project.getCreatedData());
        projectDto.setStartDate(project.getStartDate());
        projectDto.setEndDate(project.getEndDate());
        projectDto.setStatus(project.getStatus());
        return projectDto;
    }

    @NotNull
    @Override
    public Project transferProjectDtoToProject(@NotNull final ProjectDto projectDto, @NotNull final User user) {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setUser(user);
        project.setCreatedData(projectDto.getCreatedData());
        project.setStartDate(projectDto.getStartDate());
        project.setEndDate(projectDto.getEndDate());
        project.setStatus(projectDto.getStatus());
        return project;
    }

    @Override
    public List<ProjectDto> transferListProjectToListProjectDto(@NotNull List<Project> projectList) {
        @NotNull final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            projectDtoList.add(transferProjectToProjectDto(project));
        }
        return projectDtoList;
    }
}
