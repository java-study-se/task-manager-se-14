package com.morozov.tm.api;


import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId);

    List<Task> findAllByUserId(@NotNull final String userId);

    Task findOneByUserId(@NotNull final String userId, @NotNull final String id);

    List<Task> getAllTaskByProjectId(@NotNull final String projectId);

    void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId);

    List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string);

    void removeAllByUserId(@NotNull final String userId);
}
