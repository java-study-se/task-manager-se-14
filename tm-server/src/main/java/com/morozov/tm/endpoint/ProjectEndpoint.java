package com.morozov.tm.endpoint;

import com.morozov.tm.api.IProjectService;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.dto.ProjectDto;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectEndpoint {
    private IProjectService projectService;
    private ISessionService sessionService;
    private IUserService userService;

    public ProjectEndpoint(@NotNull final Bootstrap bootstrap) {
        this.projectService = bootstrap.getProjectService();
        this.sessionService = bootstrap.getSessionService();
        this.userService = bootstrap.getUserService();
    }

    public List<ProjectDto> findAllProject(
            @NotNull @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Project> allProject = projectService.findAllProject();
        return projectService.transferListProjectToListProjectDto(allProject);
    }

    @WebMethod
    public List<ProjectDto> findAllProjectByUserId(
            @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Project> allProjectByUserId = projectService.getAllProjectByUserId(sessionUser.getId());
        return projectService.transferListProjectToListProjectDto(allProjectByUserId);
    }

    @WebMethod
    public ProjectDto addProject(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectName") String projectName)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        Project project = projectService.addProject(projectName, sessionUser);
        return projectService.transferProjectToProjectDto(project);
    }

    @WebMethod
    public boolean removeProjectById(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectId") String id)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        return projectService.removeProjectById(sessionUser.getId(), id);
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectId") String id,
            @NotNull @WebParam(name = "projectName") String projectName,
            @NotNull @WebParam(name = "projectDescription") String projectDescription,
            @NotNull @WebParam(name = "dataStartProject") String dataStart,
            @NotNull @WebParam(name = "dataEndProject") String dataEnd)
            throws StringEmptyException, ParseException, ProjectNotFoundException,
            CloneNotSupportedException, AccessFirbidenException, ConnectionLostException ,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        projectService.updateProject(id, projectName, projectDescription, dataStart, dataEnd, sessionUser);
    }

    @WebMethod
    public List<ProjectDto> findProjectByStringInNameOrDescription(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "string") String string)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Project> projectBySearchString = projectService.findProjectByStringInNameOrDescription(sessionUser.getId(), string);
        return projectService.transferListProjectToListProjectDto(projectBySearchString);
    }

    @WebMethod
    public void removeAllProjectByUserId(
            @WebParam(name = "userId") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        projectService.removeAllByUserId(sessionUser.getId());
    }

    @WebMethod
    public void clearProjectList(
            @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        projectService.clearProjectList();
    }
}
