package com.morozov.tm.api;


import com.morozov.tm.entity.User;
import org.jetbrains.annotations.NotNull;

public interface IUserRepository extends IRepository<User> {
    User findOneByLogin(@NotNull final String login);
}

