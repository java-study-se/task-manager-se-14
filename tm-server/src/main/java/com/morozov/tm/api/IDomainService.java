package com.morozov.tm.api;

import com.morozov.tm.dto.DomainDto;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;

public interface IDomainService {
    void load(@NotNull DomainDto domainDto) throws ConnectionLostException;
    void export(@NotNull DomainDto domainDto) throws ConnectionLostException;
}
