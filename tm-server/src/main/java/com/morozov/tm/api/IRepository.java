package com.morozov.tm.api;

import com.morozov.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IRepository<T extends AbstractEntity> {
    List<T> findAll();

    T findOne(@NotNull final String id);

    void persist(@NotNull final T writeEntity);

    void merge(@NotNull final T updateEntity);

    void remove(T removeEntity);

    void removeAll();
}
