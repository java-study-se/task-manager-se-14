package com.morozov.tm.endpoint;

import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.dto.UserDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class UserEndpoint {

    public UserEndpoint(Bootstrap bootstrap) {
        this.userService = bootstrap.getUserService();
        this.sessionService = bootstrap.getSessionService();
    }

    private IUserService userService;
    private ISessionService sessionService;

    @WebMethod
    public void registryUser(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password)
            throws UserExistException, StringEmptyException, ConnectionLostException {
        userService.registryUser(login, password);
    }

    @WebMethod
    public void updateUserPassword(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "newPassword") String newPassword)
            throws StringEmptyException, UserNotFoundException, CloneNotSupportedException, AccessFirbidenException,
            ConnectionLostException {
        if(sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto,sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) return;
        userService.updateUserPassword(currentSession.getUser().getId(), newPassword);
    }

    @WebMethod
    public void updateUserProfile(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "newUserName") String newUserName,
            @NotNull @WebParam(name = "newUserPassword") String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, CloneNotSupportedException,
            AccessFirbidenException, ConnectionLostException {
        if(sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto,sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        userService.updateUserProfile(sessionUser.getId(), newUserName, newUserPassword);
    }

    @WebMethod
    public UserDto findOneUserById(
            @WebParam(name = "session") SessionDto sessionDto)
            throws UserNotFoundException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException {
        if(sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto,sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        return userService.transferUserToUserDto(sessionUser);
    }
}
