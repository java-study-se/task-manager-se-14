package com.morozov.tm.repository;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProjectRepository implements IProjectRepository {
    @NotNull
    private final EntityManager em;

    public ProjectRepository(@NotNull EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Project> findAll() {
        return em.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @Override
    public Project findOne(@NotNull String id) {
        return em.find(Project.class, id);
    }

    @Override
    public void persist(@NotNull Project writeEntity) {
        em.persist(writeEntity);
    }

    @Override
    public void merge(@NotNull Project updateEntity) {
        em.merge(updateEntity);
    }

    @Override
    public void remove(Project removeEntity) {
        em.remove(removeEntity);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Project",Project.class);
    }

    @Override
    public List<Project> findAllByUserId(@NotNull String userId) {
        TypedQuery<Project> query = em.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public Project findOneByUserId(@NotNull String userId, @NotNull String id) {
        TypedQuery<Project> query = em.createQuery("SELECT p FROM Project p WHERE p.id = :id AND p.user.id = :userId", Project.class);
        query.setParameter("id", id);
        query.setParameter("userId", userId);
        return query.getSingleResult();
    }

    @Override
    public List<Project> findProjectByStringInNameOrDescription(@NotNull String userId, @NotNull String string) {
        final TypedQuery<Project> query = em.createQuery("SELECT p FROM Project p WHERE p.user.id = :userId AND " +
                "(p.name LIKE CONCAT('%',:string,'%') OR p.description LIKE CONCAT('%',:stringNext,'%'))", Project.class);
        query.setParameter("userId", userId);
        query.setParameter("string", string);
        query.setParameter("stringNext", string);
        return query.getResultList();

    }

    @Override
    public void removeAllByUserId(@NotNull String userId) {
        Query query = em.createQuery("DELETE p FROM Project p WHERE p.user.id = :userId",Project.class);
        query.setParameter("userId", userId);
        query.executeUpdate();
    }
}
