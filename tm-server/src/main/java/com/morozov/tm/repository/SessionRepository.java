package com.morozov.tm.repository;

import com.morozov.tm.api.ISessionRepository;
import com.morozov.tm.entity.Session;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository implements ISessionRepository {
    @NotNull
    private final EntityManager em;

    public SessionRepository(@NotNull EntityManager em) {
        this.em = em;
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return em.createQuery("SELECT s FROM Session s", Session.class).getResultList();
    }

    @Override
    public Session findOne(@NotNull String id) {
        return em.find(Session.class, id);
    }

    @Override
    public void persist(@NotNull Session writeEntity) {
        em.persist(writeEntity);
    }

    @Override
    public void merge(@NotNull Session updateEntity) {
        em.merge(updateEntity);
    }

    @Override
    public void remove(Session removeEntity) {
        em.remove(removeEntity);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Session", Session.class);
    }
}
