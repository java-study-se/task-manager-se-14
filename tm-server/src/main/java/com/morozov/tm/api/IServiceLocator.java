package com.morozov.tm.api;


import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();
}
