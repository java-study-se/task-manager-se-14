package com.morozov.tm.endpoint;

import com.morozov.tm.api.IProjectService;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.ITaskService;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.dto.TaskDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService
public class TaskEndpoint {
    private ITaskService taskService;
    private ISessionService sessionService;
    private IProjectService projectService;
    private IUserService userService;

    public TaskEndpoint(@NotNull final Bootstrap bootstrap) {
        this.taskService = bootstrap.getTaskService();
        this.sessionService = bootstrap.getSessionService();
        this.projectService = bootstrap.getProjectService();
        this.userService = bootstrap.getUserService();
    }

    @WebMethod
    public List<TaskDto> findAllTaskByUserId(
            @NotNull @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        @NotNull final List<Task> allTaskByUserId = taskService.getAllTaskByUserId(sessionUser.getId());
        return taskService.transferListTaskToListTaskDto(allTaskByUserId);
    }

    @WebMethod
    public TaskDto addTask(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "taskName") String taskName,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Project project = projectService.findOneById(projectId);
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        Task task = taskService.addTask(sessionUser, taskName, project);
        return taskService.transferTaskToTaskDto(task);
    }


    @WebMethod
    public boolean removeTaskById(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "taskId") String taskId)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        return taskService.removeTaskById(sessionUser.getId(), taskId);
    }

    @WebMethod
    public void updateTask(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "taskId") String taskId,
            @NotNull @WebParam(name = "taskName") String taskName,
            @NotNull @WebParam(name = "taskDescription") String taskDescription,
            @NotNull @WebParam(name = "dataStartTask") String dataStart,
            @NotNull @WebParam(name = "dataEndTask") String dataEnd,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws StringEmptyException, ParseException, TaskNotFoundException, CloneNotSupportedException,
            AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Project project = projectService.findOneById(projectId);
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        taskService.updateTask(sessionUser, taskId, taskName, taskDescription,
                dataStart, dataEnd, project);
    }

    @WebMethod
    public List<TaskDto> findAllTaskByProjectId(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException,
            ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Task> allTaskByProjectId = taskService.getAllTaskByProjectId(sessionUser.getId(), projectId);
        return taskService.transferListTaskToListTaskDto(allTaskByProjectId);
    }

    @WebMethod
    public void removeAllTaskByProjectId(
            @NotNull @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        taskService.removeAllTaskByProjectId(sessionUser.getId(), projectId);
    }

    @WebMethod
    public void clearTaskList(
            @NotNull @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        taskService.clearTaskList();
    }

    @WebMethod
    public List<TaskDto> findTaskByStringInNameOrDescription(
            @WebParam(name = "session") SessionDto sessionDto,
            @NotNull @WebParam(name = "string") String string)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        List<Task> taskBySearchString = taskService.findTaskByStringInNameOrDescription(sessionUser.getId(), string);
        return taskService.transferListTaskToListTaskDto(taskBySearchString);
    }

    @WebMethod
    public void removeAllTaskByUserId(
            @WebParam(name = "session") SessionDto sessionDto)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, UserNotFoundException {
        if (sessionDto.getUserId() == null) throw new AccessFirbidenException();
        @NotNull final User sessionUser = userService.findOneById(sessionDto.getUserId());
        @NotNull final Session session = sessionService.transferSessionDtoToSession(sessionDto, sessionUser);
        @NotNull final Session currentSession = sessionService.validate(session);
        if (currentSession.getUser() == null) throw new AccessFirbidenException();
        taskService.removeAllTaskByUserId(sessionUser.getId());
    }
}
