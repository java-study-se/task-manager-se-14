package com.morozov.tm.repository;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class TaskRepository implements ITaskRepository {
    @NotNull
    private final EntityManager em;

    public TaskRepository(@NotNull EntityManager em) {
        this.em = em;
    }


    @Override
    public List<Task> findAll() {
        return em.createQuery("SELECT t FROM Task t", Task.class).getResultList();
    }

    @Override
    public Task findOne(@NotNull String id) {
        return em.find(Task.class, id);
    }

    @Override
    public void persist(@NotNull Task writeEntity) {
        em.persist(writeEntity);
    }

    @Override
    public void merge(@NotNull Task updateEntity) {
        em.merge(updateEntity);
    }

    @Override
    public void remove(Task removeEntity) {
        em.remove(removeEntity);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Task");
    }

    @Override
    public List<Task> findAllByProjectIdUserId(@NotNull String userId, @NotNull String projectId) {
        TypedQuery<Task> query = em.createQuery("SELECT t FROM Task t WHERE t.project.id = :projectId AND t.user.id = :userId", Task.class);
        query.setParameter("projectId", projectId);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public List<Task> findAllByUserId(@NotNull String userId) {
        TypedQuery<Task> query = em.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public Task findOneByUserId(@NotNull String userId, @NotNull String id) {
        TypedQuery<Task> query = em.createQuery("SELECT t FROM Task t WHERE t.id = :id and t.user.id = :userId", Task.class);
        query.setParameter("id", id);
        query.setParameter("userId", userId);
        return query.getSingleResult();
    }

    @Override
    public List<Task> getAllTaskByProjectId(@NotNull String projectId) {
        TypedQuery<Task> query = em.createQuery("SELECT t FROM Task t WHERE t.project.id = :projectId", Task.class);
        query.setParameter("projectId", projectId);
        return query.getResultList();
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        TypedQuery<Task> query = em.createQuery("DELETE FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId",Task.class);
        query.executeUpdate();
    }

    @Override
    public List<Task> findTaskByStringInNameOrDescription(@NotNull String userId, @NotNull String string) {
        TypedQuery<Task> query = em.createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND " +
                "(t.name LIKE CONCAT ('%',:string1,'%') OR t.description LIKE CONCAT ('%',:string2,'%'))", Task.class);
        query.setParameter("userId", userId);
        query.setParameter("string1", string);
        query.setParameter("string2", string);
        return query.getResultList();
    }

    @Override
    public void removeAllByUserId(@NotNull String userId) {
        TypedQuery<Task> query = em.createQuery("DELETE FROM Task t WHERE t.user.id = :userId", Task.class);
        query.executeUpdate();
    }
}
