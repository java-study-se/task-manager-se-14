package com.morozov.tm.api;


import com.morozov.tm.dto.TaskDto;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    List<Task> findAllTask() throws ConnectionLostException;

    List<Task> getAllTaskByUserId(@NotNull String userId) throws ConnectionLostException;

    Task addTask(User user, @NotNull String taskName, Project project)
            throws StringEmptyException, ConnectionLostException;

    boolean removeTaskById(@NotNull String userId, @NotNull String id) throws StringEmptyException, ConnectionLostException;

    void updateTask(User user, @NotNull String id, @NotNull String name, @NotNull String description,
                    @NotNull String dataStart, @NotNull String dataEnd, Project project)
            throws StringEmptyException, ParseException, TaskNotFoundException, ConnectionLostException;

    List<Task> getAllTaskByProjectId(@NotNull String userId, @NotNull String projectId)
            throws StringEmptyException, ConnectionLostException;

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws ConnectionLostException;

    void clearTaskList() throws ConnectionLostException;

    List<Task> findTaskByStringInNameOrDescription(@NotNull String userId, @NotNull String string)
            throws ConnectionLostException;

    void removeAllTaskByUserId(@NotNull String userId) throws ConnectionLostException;

    void loadTaskList(@Nullable final List<Task> loadList) throws ConnectionLostException;

    @NotNull
    TaskDto transferTaskToTaskDto(@NotNull final Task task);

    Task transferTaskDtoToTask(@NotNull final TaskDto taskDto, @NotNull final User user, @NotNull final Project project);

    @NotNull
    List<TaskDto> transferListTaskToListTaskDto(@NotNull final List<Task> taskList);

}
