package com.morozov.tm.api;

import com.morozov.tm.dto.UserDto;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;


import java.util.List;

public interface IUserService {
    User loginUser(@NotNull String login, @NotNull String password) throws UserNotFoundException, StringEmptyException, ConnectionLostException;

    String registryUser(@NotNull String login, @NotNull String password) throws UserExistException, StringEmptyException, ConnectionLostException;

    void updateUserPassword(@NotNull String id, @NotNull String newPassword) throws StringEmptyException, UserNotFoundException, ConnectionLostException;

    void updateUserProfile(@NotNull String id, @NotNull String newUserName, @NotNull String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, ConnectionLostException;

    void loadUserList(final List<User> userList) throws ConnectionLostException;

    List<User> findAllUser() throws ConnectionLostException;

    User findOneById(@NotNull final String id) throws UserNotFoundException, ConnectionLostException;

    UserDto transferUserToUserDto(@NotNull final User user);

    List<UserDto> transferListUserToListUserDto(@NotNull final List<User> userList);
}
