package com.morozov.tm.util;


import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.Task;
import com.morozov.tm.entity.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class EntityManagerFactoryUtil {
    public static EntityManagerFactory getFactory() {
        @NotNull final Properties dbConnectionProperties = new Properties();
        @Nullable FileInputStream fis;
        @NotNull final Map<String, String> settings = new HashMap<>();
        try {
            fis = new FileInputStream("tm-server/src/main/resources//dbconfig.properties");
            dbConnectionProperties.load(fis);
            settings.put(Environment.DRIVER, dbConnectionProperties.getProperty("db.driver"));
            settings.put(Environment.URL, dbConnectionProperties.getProperty("db.url"));
            settings.put(Environment.PASS, dbConnectionProperties.getProperty("db.password"));
            settings.put(Environment.USER, dbConnectionProperties.getProperty("db.username"));
            settings.put(Environment.DIALECT, dbConnectionProperties.getProperty("db.dialect.mysql"));
            settings.put(Environment.HBM2DDL_AUTO, dbConnectionProperties.getProperty("db.hbm2ddl_auto"));
            settings.put(Environment.SHOW_SQL, dbConnectionProperties.getProperty("db.show_sql"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }


}
