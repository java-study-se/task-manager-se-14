package com.morozov.tm.service;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.dto.UserDto;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.repository.UserRepository;
import com.morozov.tm.util.DataValidationUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

public final class UserService implements IUserService {
    @Nullable
    final private EntityManagerFactory factory;


    public UserService(@Nullable EntityManagerFactory factory) {
        this.factory = factory;
    }

    @Override
    public User loginUser(@NotNull final String login, @NotNull final String password)
            throws UserNotFoundException, StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(login, password);
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        @Nullable User user;
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(em);
            user = userRepository.findOneByLogin(login);
        }catch (NoResultException e){
            e.getMessage();
            throw new UserNotFoundException();
        } finally {
            em.close();
        }
        final String typePasswordHash = MD5HashUtil.getHash(password);
        if (typePasswordHash == null) throw new UserNotFoundException();
        if (!typePasswordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public String registryUser(@NotNull final String login, @NotNull final String password)
            throws UserExistException, StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(login, password);
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        @NotNull final String userId;
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(em);
            @Nullable User oneByLogin = null;
            try {
                oneByLogin = userRepository.findOneByLogin(login);
            } catch (NoResultException e) {
            }
            if (oneByLogin != null) throw new UserExistException();
            @NotNull final User user = new User();
            user.setLogin(login);
            user.setRole(UserRoleEnum.USER);
            final String hashPassword = MD5HashUtil.getHash(password);
            if (hashPassword == null) throw new StringEmptyException();
            user.setPasswordHash(hashPassword);
            userRepository.persist(user);
            em.getTransaction().commit();
            userId = user.getId();
        } finally {
            em.close();
        }
        return userId;
    }


    @Override
    public void updateUserPassword(@NotNull final String id, @NotNull final String newPassword) throws StringEmptyException, UserNotFoundException, ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(em);
            @Nullable User user = userRepository.findOne(id);
            if (user != null && !user.getId().equals(id)) throw new UserNotFoundException();
            DataValidationUtil.checkEmptyString(newPassword);
            if (user == null) throw new UserNotFoundException();
            String hashNewPassword = MD5HashUtil.getHash(newPassword);
            if (hashNewPassword != null) {
                user.setPasswordHash(hashNewPassword);
            }
            userRepository.merge(user);
            em.getTransaction().commit();
        } finally {
            em.close();
        }

    }

    @Override
    public void updateUserProfile(@NotNull final String id, @NotNull final String newUserName, @NotNull final String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(em);
            @Nullable User user = userRepository.findOne(id);
            if (user != null && !user.getId().equals(id)) throw new UserNotFoundException();
            DataValidationUtil.checkEmptyString(newUserName, newUserPassword);
            user.setLogin(newUserName);
            String hashNewUserPassword = MD5HashUtil.getHash(newUserPassword);
            if (hashNewUserPassword != null) {
                user.setPasswordHash(hashNewUserPassword);
            }
            userRepository.merge(user);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    public void loadUserList(final List<User> userList) throws ConnectionLostException {
        if (userList != null) {
            for (User user : userList) {
                if (factory == null) throw new ConnectionLostException();
                @Nullable final EntityManager em = factory.createEntityManager();
                if (em == null) throw new ConnectionLostException();
                try {
                    em.getTransaction().begin();
                    @NotNull final IUserRepository userRepository = new UserRepository(em);
                    userRepository.persist(user);
                    em.getTransaction().commit();
                } finally {
                    em.close();
                }
            }
        }
    }

    @Override
    public List<User> findAllUser() throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(em);
            return userRepository.findAll();
        } finally {
            em.close();
        }
    }

    @Override
    public User findOneById(@NotNull String id) throws UserNotFoundException, ConnectionLostException {
        @Nullable User user;
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(em);
            user = userRepository.findOne(id);
        } finally {
            em.close();
        }
        if (user == null) throw new UserNotFoundException();
        if (!user.getId().equals(id)) throw new UserNotFoundException();
        return user;
    }

    @Override
    @NotNull
    public UserDto transferUserToUserDto(@NotNull final User user) {
        @NotNull final UserDto resultUserDto = new UserDto();
        resultUserDto.setId(user.getId());
        resultUserDto.setLogin(user.getLogin());
        resultUserDto.setPasswordHash(user.getPasswordHash());
        resultUserDto.setRole(user.getRole());
        return resultUserDto;
    }

    @Override
    @NotNull
    public List<UserDto> transferListUserToListUserDto(@NotNull final List<User> userList) {
        List<UserDto> resultList = new ArrayList<>();
        for (@NotNull final User user : userList) {
            resultList.add(transferUserToUserDto(user));
        }
        return resultList;
    }

}
