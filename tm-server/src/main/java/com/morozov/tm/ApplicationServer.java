package com.morozov.tm;


import com.morozov.tm.service.Bootstrap;

public class ApplicationServer {

    public static void main(String[] args) {
        new Bootstrap().init();
    }
}
