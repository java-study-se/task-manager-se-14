package com.morozov.tm.api;

import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;


public interface ISessionService {

    Session getNewSession(final User user) throws ConnectionLostException;

    Session validate(final Session session) throws AccessFirbidenException, CloneNotSupportedException, ConnectionLostException;

    void closeSession(@NotNull final Session session) throws ConnectionLostException;

    SessionDto transferSessionToSessionDto(@NotNull final Session session, User user);

    Session transferSessionDtoToSession(@NotNull final SessionDto sessionDto, @NotNull final User user);
}
