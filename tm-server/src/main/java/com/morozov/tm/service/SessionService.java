package com.morozov.tm.service;

import com.morozov.tm.api.ISessionRepository;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.dto.SessionDto;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.repository.SessionRepository;
import com.morozov.tm.util.SessionUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import java.util.Date;

public class SessionService implements ISessionService {
    @Nullable
    final private EntityManagerFactory factory;

    public SessionService(@Nullable EntityManagerFactory factory) {
        this.factory = factory;
    }

    @Override
    public Session getNewSession(User user) throws ConnectionLostException {
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(new Date());
        Session sessionWithSign = SessionUtil.sign(session);
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.persist(session);
            em.getTransaction().commit();
        } finally {
            em.close();
        }

        return sessionWithSign;
    }

    @Override
    public Session validate(@Nullable final Session session) throws AccessFirbidenException, CloneNotSupportedException, ConnectionLostException {
        if (session == null) throw new AccessFirbidenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessFirbidenException();
        if (session.getUser() == null) throw new AccessFirbidenException();
        if (session.getTimestamp() == null || !SessionUtil.isSessionTimeLive(session.getTimestamp()))
            throw new AccessFirbidenException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessFirbidenException();
        @NotNull final String signatureOrigin = session.getSignature();
        @Nullable final String signatureTemp = SessionUtil.sign(temp).getSignature();
        final boolean check = signatureOrigin.equals(signatureTemp);
        if (!check) throw new AccessFirbidenException();
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.findOne(session.getId());
        }catch (NoResultException e){
            e.printStackTrace();
            throw new AccessFirbidenException();
        }
        finally {
            em.close();
        }

        return session;
    }

    @Override
    public void closeSession(@NotNull final Session session) throws ConnectionLostException {
        if (factory == null) throw new ConnectionLostException();
        @Nullable final EntityManager em = factory.createEntityManager();
        if (em == null) throw new ConnectionLostException();
        try {
            em.getTransaction().begin();
            @Nullable final ISessionRepository sessionRepository = new SessionRepository(em);
            sessionRepository.remove(session);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public SessionDto transferSessionToSessionDto(@NotNull final Session session, User user) {
        @NotNull final SessionDto sessionDto = new SessionDto();
        sessionDto.setId(session.getId());
        sessionDto.setSignature(session.getSignature());
        sessionDto.setTimestamp(session.getTimestamp());
        sessionDto.setUserId(session.getUser().getId());
        sessionDto.setUserRoleEnum(user.getRole());
        return sessionDto;
    }

    @Override
    public Session transferSessionDtoToSession(@NotNull SessionDto sessionDto, @NotNull User user) {
        @NotNull final Session session = new Session();
        session.setId(sessionDto.getId());
        session.setSignature(sessionDto.getSignature());
        session.setTimestamp(sessionDto.getTimestamp());
        session.setUser(user);
        return session;
    }
}
