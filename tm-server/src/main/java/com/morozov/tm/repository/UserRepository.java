package com.morozov.tm.repository;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;

import javax.persistence.TypedQuery;
import java.util.List;

public class UserRepository implements IUserRepository {
    @NotNull
    private final EntityManager em;

    public UserRepository(@NotNull EntityManager em) {
        this.em = em;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final List<User> resultList = em.createQuery("SELECT u FROM User u",User.class).getResultList();
        return resultList;
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) {
        return em.find(User.class, id);
    }

    @Override
    public void persist(@NotNull final User writeEntity) {
        em.persist(writeEntity);
    }

    @Override
    public void merge(@NotNull final User updateEntity) {
        em.merge(updateEntity);
    }

    @Override
    public void remove(@NotNull final User removeEntity) {
        em.remove(removeEntity);
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM User",User.class);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        TypedQuery<User> query = em.createQuery("SELECT u FROM User u where u.login = :login", User.class);
        query.setParameter("login", login);
        return query.getSingleResult();
    }
}
