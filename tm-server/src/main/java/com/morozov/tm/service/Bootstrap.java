package com.morozov.tm.service;

import com.morozov.tm.api.*;
import com.morozov.tm.util.EndpointPublishUtil;
import com.morozov.tm.util.EntityManagerFactoryUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManagerFactory;


public final class Bootstrap implements IServiceLocator {
    @Nullable
    final private EntityManagerFactory factory = EntityManagerFactoryUtil.getFactory();
    @NotNull
    final private IProjectService projectService = new ProjectService(factory);
    @NotNull
    final private ITaskService taskService = new TaskService(factory);
    @NotNull
    final private IUserService userService = new UserService(factory);
    @NotNull
    final private IDomainService domainService = new DomainService(this);
    @NotNull
    final private ISessionService sessionService = new SessionService(factory);

    public void init() {
        EndpointPublishUtil.publish(this);
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }
}

