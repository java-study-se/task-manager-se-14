package com.morozov.tm.api;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IServiceLocator {

    List<AbstractCommand> getCommandList();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    DomainEndpoint getDomainEndpoint();

    @Nullable SessionDto getSession();

    void setSession(@Nullable SessionDto session);
    @NotNull
    SessionEndpoint getSessionEndpoint();

}
